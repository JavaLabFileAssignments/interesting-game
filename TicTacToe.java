package interesting_game;
import java.util.*;

public class TicTacToe {
    private static ArrayList<Integer> playerPositions = new ArrayList<Integer>();
    private static ArrayList<Integer> cpuPositions = new ArrayList<Integer>();
    private static Scanner key = new Scanner(System.in);

    public static void main(String[] args) {
        char[][] gameBoard = {
                {' ', '|', ' ', '|', ' '},
                {'-', '+', '-', '+', '-'},
                {' ', '|', ' ', '|', ' '},
                {'-', '+', '-', '+', '-'},
                {' ', '|', ' ', '|', ' '}
        };

        printGameBoard(gameBoard);

        while (true) {
            System.out.println("Enter your placement (1-9):");
            int positionOfPlayer = key.nextInt();
            while (playerPositions.contains(positionOfPlayer)||cpuPositions.contains(positionOfPlayer)){
                System.out.println("Position is taken! Enter another position:");
                positionOfPlayer = key.nextInt();
            }

            placePiece(gameBoard, positionOfPlayer, "player");
//
//            String winner = checkWinner();
//            if (winner.length() > 0){
//                System.out.println(winner);
//                break;
//            }

            Random random = new Random();
            int positionOfCpu = random.nextInt(9) + 1;
            while (playerPositions.contains(positionOfCpu)||cpuPositions.contains(positionOfCpu)){
                positionOfCpu = random.nextInt(9) + 1;
            }
            placePiece(gameBoard, positionOfCpu, "cpu");

            printGameBoard(gameBoard);

            String lastResult = checkWinner();
            System.out.println(lastResult);
//            if (winner.length() > 0){
//                System.out.println(winner);
//                break;
//            }
        }

    }

    private static void printGameBoard(char[][] gameBoard) {
        for (char[] row : gameBoard) {
            for (char column : row) {
                System.out.print(column);
            }
            System.out.println();
        }
    }

    private static void placePiece(char[][] gameBoard, int position, String user) {
        char symbol = ' ';

        if (user.equals("player")) {
            symbol = 'X';
            playerPositions.add(position);
        } else if (user.equals("cpu")) {
            symbol = 'Y';
            cpuPositions.add(position);
        }
        switch (position) {
            case 1:
                gameBoard[0][0] = symbol;
                break;
            case 2:
                gameBoard[0][2] = symbol;
                break;
            case 3:
                gameBoard[0][4] = symbol;
                break;
            case 4:
                gameBoard[2][0] = symbol;
                break;
            case 5:
                gameBoard[2][2] = symbol;
                break;
            case 6:
                gameBoard[2][4] = symbol;
                break;
            case 7:
                gameBoard[4][0] = symbol;
                break;
            case 8:
                gameBoard[4][2] = symbol;
                break;
            case 9:
                gameBoard[4][4] = symbol;
                break;
            default:
                break;
        }
    }

    private static String checkWinner(){
        List topRow = Arrays.asList(1,2,3);
        List middleRow = Arrays.asList(4,5,6);
        List bottomRow = Arrays.asList(7,8,9);

        List leftColumn = Arrays.asList(1,4,7);
        List middleColumn = Arrays.asList(2,5,8);
        List rightColumn = Arrays.asList(3,6,9);

        List firstCross = Arrays.asList(1,5,9);
        List secondCroos = Arrays.asList(3,5,7);

        List<List> winner = new ArrayList<List>();
        winner.add(topRow);
        winner.add(middleRow);
        winner.add(bottomRow);
        winner.add(leftColumn);
        winner.add(middleColumn);
        winner.add(rightColumn);
        winner.add(firstCross);
        winner.add(secondCroos);

        for (List winning : winner){
            if (playerPositions.containsAll(winning)){
                return "Congratulation! You won....";
            }else if (cpuPositions.containsAll(winning)){
                return "You lose! CPU won....";
            }else if (playerPositions.size() + cpuPositions.size() == 9){
                return "Nobody won!";
            }
        }
        return " ";
    }
}